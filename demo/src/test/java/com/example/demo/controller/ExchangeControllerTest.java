/**
 * Philips Electronics N.V. 2020. All rights are reserved. Reproduction or transmission in whole or in part,
 * in any form or by any means, electronic, mechanical or otherwise, is prohibited without the prior written
 * consent of the copyright owner.
 **/

package com.example.demo.controller;

import com.example.demo.dto.ExchangeEntityReqDto;
import com.example.demo.dto.ExchangeEntityResDto;
import com.example.demo.entities.UrlStringEntity;
import com.example.demo.service.impl.ExchangeServiceImpl;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.text.ParseException;

import static org.mockito.ArgumentMatchers.any;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * This class is used to handle requests for the ExchangeControllerTest entity.
 */
@RunWith(SpringRunner.class)
@WebMvcTest(ExchangeController.class)
public class ExchangeControllerTest {
    @Autowired
    private MockMvc mvc;

    @Autowired
    private ObjectMapper objectMapper;

    @MockBean
    private ExchangeServiceImpl exchangeService;

    private ExchangeEntityReqDto exchangeEntityReqDto;
    private ExchangeEntityResDto exchangeEntityResDto;
    private UrlStringEntity urlStringEntity;

    @Before
    public void setUp() throws ParserConfigurationException, SAXException, ParseException, IOException {
        exchangeEntityReqDto = createExchangeEntityReqDto();
        exchangeEntityResDto = createExchangeEntityResDto();
        urlStringEntity = createUrlStringEntity();

        Mockito.when(exchangeService.exchangeResult(any(ExchangeEntityReqDto.class))).thenReturn(exchangeEntityResDto);
        Mockito.doNothing().when(exchangeService).createDateRateMap(any(UrlStringEntity.class));
    }

    @Test
    public void calculateExchangeTest() throws Exception {
        mvc.perform(MockMvcRequestBuilders
                .post("/calculateExchange")
                .content(objectMapper.writeValueAsString(exchangeEntityReqDto))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    public void loadXMLWebAddressTest() throws Exception {
        mvc.perform(MockMvcRequestBuilders
                .post("/calculateExchange/loadXml")
                .content(objectMapper.writeValueAsString(urlStringEntity))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    private ExchangeEntityReqDto createExchangeEntityReqDto(){
        ExchangeEntityReqDto reqDto = new ExchangeEntityReqDto();
        reqDto.setAmount("100");
        reqDto.setComission("0.1%");
        reqDto.setCurrentDate("12.9.2021");
        reqDto.setFromCurrency("$");
        reqDto.setToCurrency("E");
        return reqDto;
    }
    private ExchangeEntityResDto createExchangeEntityResDto(){
        ExchangeEntityResDto resDto = new ExchangeEntityResDto();
        resDto.setAmount("100");
        resDto.setAmountResult("150");
        resDto.setComission("0.1%");
        resDto.setCurrentDate("12.9.2021");
        resDto.setFromCurrency("$");
        resDto.setToCurrency("E");
        return resDto;
    }
    private UrlStringEntity createUrlStringEntity(){
        UrlStringEntity urlStringEntity = new UrlStringEntity();
        urlStringEntity.setUrl("www.bnr.ro/xml");
        return urlStringEntity;
    }
}