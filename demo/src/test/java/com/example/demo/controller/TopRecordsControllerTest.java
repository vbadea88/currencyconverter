/**
 * Philips Electronics N.V. 2020. All rights are reserved. Reproduction or transmission in whole or in part,
 * in any form or by any means, electronic, mechanical or otherwise, is prohibited without the prior written
 * consent of the copyright owner.
 **/

package com.example.demo.controller;

import com.example.demo.dto.TopRecordsReqDto;
import com.example.demo.dto.TopRecordsResDto;
import com.example.demo.service.impl.TopRecordsServiceImpl;
import com.example.demo.utils.DateCurrencyObject;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Date;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * This class is used to handle requests for the TopRecordsControllerTest entity.
 */
@RunWith(SpringRunner.class)
@WebMvcTest(TopRecordsController.class)
public class TopRecordsControllerTest {
    @Autowired
    private MockMvc mvc;

    @MockBean
    private TopRecordsServiceImpl topRecordsService;

    @Autowired
    private ObjectMapper objectMapper;

    private TopRecordsResDto topRecordsResDto;
    private TopRecordsReqDto topRecordsReqDto;

    @Before
    public void setUp(){
        topRecordsResDto = createTopRecordsDro();
        topRecordsReqDto = createTopRecordsReqDto();
        Mockito.when(topRecordsService.createResult(topRecordsReqDto)).thenReturn(topRecordsResDto);
    }

    @Test
    public void getTopRecordsTest() throws Exception {
        mvc.perform(MockMvcRequestBuilders
                .post("/calculateExchange/getTopRecords")
                .content(objectMapper.writeValueAsString(topRecordsReqDto))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    private TopRecordsResDto createTopRecordsDro(){
        TopRecordsResDto topRecordsResDto = new TopRecordsResDto();
        topRecordsResDto.setCurrency("Ron");
        topRecordsResDto.setOrder("Min");
        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
        try {
            Date day = formatter.parse("15-09-2021");
            DateCurrencyObject dateCurrencyObject = new DateCurrencyObject(day,0.5F );
            topRecordsResDto.setDateCurrencyObjects(Collections.singletonList(dateCurrencyObject));
        } catch (ParseException e) {}
        return topRecordsResDto;
    }

    private TopRecordsReqDto createTopRecordsReqDto(){
        TopRecordsReqDto reqDto = new TopRecordsReqDto();
        reqDto.setCurrency("Ron");
        reqDto.setNrOfRecords("5");
        reqDto.setOrder("Min");
        return reqDto;
    }
}