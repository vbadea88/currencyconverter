/**
 * Philips Electronics N.V. 2020. All rights are reserved. Reproduction or transmission in whole or in part,
 * in any form or by any means, electronic, mechanical or otherwise, is prohibited without the prior written
 * consent of the copyright owner.
 **/

package com.example.demo.service;

import com.example.demo.dto.ExchangeEntityReqDto;
import com.example.demo.dto.ExchangeEntityResDto;
import com.example.demo.entities.ExchangeEntity;
import com.example.demo.entities.UrlStringEntity;
import com.example.demo.mapper.ExchangeMapper;
import com.example.demo.service.impl.ExchangeServiceImpl;
import com.example.demo.utils.ExchangeClass;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.test.context.junit4.SpringRunner;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
/**
 * This class is used to handle requests for the ExchangeServiceImplTest entity.
 */
@RunWith(SpringRunner.class)
public class ExchangeServiceImplTest {
    private ExchangeService exchangeService;

    private ExchangeMapper exchangeMapper = Mockito.mock(ExchangeMapper.class);
    private ExchangeClass exchangeClass = Mockito.mock(ExchangeClass.class);

    private ExchangeEntity exchangeEntity;
    private ExchangeEntityReqDto exchangeEntityReqDto;
    private ExchangeEntityResDto exchangeEntityResDto;
    private Map<Date, Map<String, Float>> dateRateMap;
    private UrlStringEntity urlStringEntity;
    private final String DAY_STRING = "15-06-2020";
    private final Date DAY_ZZ = createDate(DAY_STRING);
    private final String RON_CURRENCY = "RON";
    private final String AED_CURRENCY = "AED";
    private final Float AMOUNT = 100F;
    private final Float COMISSION = 0.1F;
    private final Float RON_TO_AED = 1.1695F;
    private final String DUMMY_URL = "http://www.oracle.com/";
    private final Map<String, Float> rateMapAed = createRateMap();

    @Before
    public void setUp() throws ParserConfigurationException, SAXException, ParseException, IOException {
        exchangeEntity = createExchangeEntity();
        exchangeEntityReqDto = createExchangeEntityReqDto();
        exchangeEntityResDto = createExchangeEntityResDto();
        dateRateMap = createDateRateMap();
        urlStringEntity = new UrlStringEntity();
        urlStringEntity.setUrl(DUMMY_URL);
        exchangeService = new ExchangeServiceImpl(exchangeMapper, exchangeClass);
    }

    @Test
    public void exchangeResultTest() throws ParserConfigurationException, SAXException, ParseException, IOException {
        Mockito.when(exchangeMapper.convertExcEntDtoToEntity(any(ExchangeEntityReqDto.class)))
                .thenReturn(exchangeEntity);
        Mockito.when(exchangeClass.getDateRateCurrencyMap()).thenReturn(dateRateMap);
        Mockito.when(exchangeMapper.convertExcEntEntityToRes(exchangeEntity,
                (AMOUNT/rateMapAed.get(AED_CURRENCY))*(1-COMISSION/100))).thenReturn(exchangeEntityResDto);

        float result  = (AMOUNT/rateMapAed.get(AED_CURRENCY))*((1-COMISSION)/100);
        ExchangeEntityResDto resDto = exchangeService.exchangeResult(exchangeEntityReqDto);
        assertEquals("The result should be the same", resDto.getAmountResult().equals(String.valueOf(result)), true);
    }

    @Test
    public void createDateRateMapTest() throws IOException, ParseException, SAXException, ParserConfigurationException {
        Mockito.doNothing().when(exchangeClass).createExchangeMap(urlStringEntity.getUrl());
        exchangeService.createDateRateMap(urlStringEntity);
        Mockito.verify(exchangeClass,times(1)).createExchangeMap(DUMMY_URL);
    }

    private ExchangeEntity createExchangeEntity(){
        ExchangeEntity exchangeEntity = new ExchangeEntity();
        exchangeEntity.setAmount(AMOUNT);
        exchangeEntity.setComission(COMISSION);
        exchangeEntity.setCurrentDate(DAY_ZZ);
        exchangeEntity.setFromCurrency(RON_CURRENCY);
        exchangeEntity.setToCurrency(AED_CURRENCY);
        return exchangeEntity;
    }

    private ExchangeEntityReqDto createExchangeEntityReqDto(){
        ExchangeEntityReqDto reqDto = new ExchangeEntityReqDto();
        reqDto.setAmount(String.valueOf(AMOUNT));
        reqDto.setComission(String.valueOf(COMISSION));
        reqDto.setCurrentDate(DAY_STRING);
        reqDto.setFromCurrency(RON_CURRENCY);
        reqDto.setToCurrency(AED_CURRENCY);
        return reqDto;
    }

    private Map<Date, Map<String, Float>> createDateRateMap(){
        Map<Date, Map<String, Float>> dateRateMap = new HashMap<>();
        dateRateMap.put(DAY_ZZ, createRateMap());
        return dateRateMap;
    }

    private Map<String, Float> createRateMap(){
        Map<String, Float> rateMap = new HashMap<>();
        rateMap.put(AED_CURRENCY, RON_TO_AED);
        return rateMap;
    }

    private final Date createDate(String date) {
        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
        try {
            return formatter.parse(date);
        } catch (ParseException e) {
            return new Date();
        }
    }

    private final ExchangeEntityResDto createExchangeEntityResDto(){
        ExchangeEntityResDto resDto = new ExchangeEntityResDto();
        resDto.setFromCurrency(RON_CURRENCY);
        resDto.setToCurrency(AED_CURRENCY);
        resDto.setComission(COMISSION.toString());
        resDto.setCurrentDate(DAY_STRING);
        resDto.setAmount(AMOUNT.toString());
        float result  = (AMOUNT/rateMapAed.get(AED_CURRENCY))*((1-COMISSION)/100);
        resDto.setAmountResult(String.valueOf(result));
        return resDto;
    }
}