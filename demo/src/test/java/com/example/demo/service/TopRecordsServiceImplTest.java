/**
 * Philips Electronics N.V. 2020. All rights are reserved. Reproduction or transmission in whole or in part,
 * in any form or by any means, electronic, mechanical or otherwise, is prohibited without the prior written
 * consent of the copyright owner.
 **/

package com.example.demo.service;

import com.example.demo.dto.TopRecordsReqDto;
import com.example.demo.dto.TopRecordsResDto;
import com.example.demo.entities.TopRecordsEntity;
import com.example.demo.mapper.TopRecordsMapper;
import com.example.demo.service.impl.TopRecordsServiceImpl;
import com.example.demo.utils.DateCurrencyObject;
import com.example.demo.utils.ExchangeClass;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.test.context.junit4.SpringRunner;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

import static org.junit.Assert.assertEquals;
/**
 * This class is used to handle requests for the TopRecordsServiceImplTest entity.
 */
@RunWith(SpringRunner.class)
public class TopRecordsServiceImplTest {
    private TopRecordsService topRecordsService;
    private ExchangeClass exchangeClass = Mockito.mock(ExchangeClass.class);
    private TopRecordsMapper topRecordsMapper = Mockito.mock(TopRecordsMapper.class);
    private final TopRecordsReqDto reqDto = createTopRecordsReqDto();
    private Map<Date, Map<String, Float>> dateRateMap;
    private final String DAY_STRING = "15-06-2020";
    private  Date DAY_ZZ = createDate(DAY_STRING);
    private final String AED_CURRENCY = "AED";
    private final Float RON_TO_AED = 1.1695F;
    private final int NR_OF_REC = 1;
    private final String MIN = "topMin";
    private final List<DateCurrencyObject> dateCurrencyObjects = createDateCurrentObjectList();
    private final TopRecordsEntity ent = createTopRecordsEntity();
    private final TopRecordsResDto res = createTopRecordsResultDto();

    @Before
    public void setUp(){
        topRecordsService = new TopRecordsServiceImpl(exchangeClass, topRecordsMapper);
        dateRateMap = createDateRateMap();
    }

    @Test
    public void createResultTest(){
        Mockito.when(topRecordsMapper.convertTopRecReqToEntity(reqDto)).thenReturn(ent);
        Mockito.when(exchangeClass.getDateRateCurrencyMap()).thenReturn(dateRateMap);
        Mockito.when(topRecordsMapper.convertTopRecEntToRes(Mockito.eq(ent),Mockito.any(List.class))).thenReturn(res);

        TopRecordsResDto topRecordsResDto = topRecordsService.createResult(reqDto);
        assertEquals("The result should be the same", topRecordsResDto.getDateCurrencyObjects().get(0).getDate().
                equals(res.getDateCurrencyObjects().get(0).getDate()), true);
    }
    private TopRecordsReqDto createTopRecordsReqDto(){
        TopRecordsReqDto reqDto = new TopRecordsReqDto();
        reqDto.setCurrency(AED_CURRENCY);
        reqDto.setNrOfRecords(String.valueOf(NR_OF_REC));
        reqDto.setOrder(MIN);
        return reqDto;
    }

    private TopRecordsEntity createTopRecordsEntity(){
        TopRecordsEntity ent = new TopRecordsEntity();
        ent.setCurrency(AED_CURRENCY);
        ent.setNrOfRecords(NR_OF_REC);
        ent.setOrder(MIN);
        return ent;
    }

    private List<DateCurrencyObject> createDateCurrentObjectList(){
        DateCurrencyObject dateCurrencyObject = new DateCurrencyObject(DAY_ZZ,RON_TO_AED);
        List<DateCurrencyObject> dateCurrencyObjects = new ArrayList<>();
        dateCurrencyObjects.add(dateCurrencyObject);
        return dateCurrencyObjects;
    }

    private Map<Date, Map<String, Float>> createDateRateMap(){
        Map<Date, Map<String, Float>> dateRateMap = new HashMap<>();
        dateRateMap.put(DAY_ZZ, createRateMap());
        return dateRateMap;
    }

    private Map<String, Float> createRateMap(){
        Map<String, Float> rateMap = new HashMap<>();
        rateMap.put(AED_CURRENCY, RON_TO_AED);
        return rateMap;
    }

    private Date createDate(String date) {
        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
        try {
            return formatter.parse(date);
        } catch (ParseException e) {
            return new Date();
        }
    }

    private  TopRecordsResDto createTopRecordsResultDto(){
        TopRecordsResDto resDto = new TopRecordsResDto();
        resDto.setOrder(MIN);
        resDto.setCurrency(AED_CURRENCY);
        resDto.setDateCurrencyObjects(dateCurrencyObjects);
        return resDto;
    }
}