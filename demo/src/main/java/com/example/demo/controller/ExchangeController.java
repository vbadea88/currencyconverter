/**
 * Philips Electronics N.V. 2020. All rights are reserved. Reproduction or transmission in whole or in part,
 * in any form or by any means, electronic, mechanical or otherwise, is prohibited without the prior written
 * consent of the copyright owner.
 **/

package com.example.demo.controller;

import com.example.demo.dto.ExchangeEntityReqDto;
import com.example.demo.dto.ExchangeEntityResDto;
import com.example.demo.entities.UrlStringEntity;
import com.example.demo.service.impl.ExchangeServiceImpl;
import com.example.demo.swagger.SwaggerConfig;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.text.ParseException;

/**
 * This class is used to handle requests for the ExchangeController entity.
 */

@RestController
@RequestMapping("/calculateExchange")
@CrossOrigin(origins = {"http://localhost:3000"})
@Api(tags = SwaggerConfig.EXCHANGE_CONTROLLER_TAG)
public class ExchangeController {

    @Autowired
    private ExchangeServiceImpl exchangeService;

    @ApiOperation(value = "Calculate the exchange rate",response = ExchangeEntityResDto.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Calculate the exchange rate"),
            @ApiResponse(code = 401, message = "You are not authorized to Calculate the exchange rate"),
            @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
            @ApiResponse(code = 404, message = "The resource you were trying to reach is not found")
    })
    @PostMapping(produces = "application/json")
    public ResponseEntity<ExchangeEntityResDto>calculateExchange(@RequestBody ExchangeEntityReqDto dto) {

        ExchangeEntityResDto resDto = exchangeService.exchangeResult(dto);
        return ResponseEntity.status(HttpStatus.OK)
                .body(resDto);
    }

    @ApiOperation(value = "Loading the exchange rate",response = String.class)
    @PostMapping(value = "/loadXml", produces = "application/json")
    public ResponseEntity<String> loadXMLWebAddress(@RequestBody UrlStringEntity url) {
        try {
            exchangeService.createDateRateMap(url);
        } catch (Exception e){
            return new ResponseEntity<>(url.getUrl() + " there was an error while loading the xml file!", HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(url.getUrl() + " was loaded succesfully!", HttpStatus.OK);
    }
}