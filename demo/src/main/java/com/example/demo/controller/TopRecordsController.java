/**
 * Philips Electronics N.V. 2020. All rights are reserved. Reproduction or transmission in whole or in part,
 * in any form or by any means, electronic, mechanical or otherwise, is prohibited without the prior written
 * consent of the copyright owner.
 **/

package com.example.demo.controller;

import com.example.demo.dto.TopRecordsReqDto;
import com.example.demo.dto.TopRecordsResDto;
import com.example.demo.service.impl.TopRecordsServiceImpl;
import com.example.demo.swagger.SwaggerConfig;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 * This class is used to handle requests for the TopRecordsController entity.
 */
@RestController
@RequestMapping("/calculateExchange")
@Api(tags = SwaggerConfig.TOP_RECORD_CONTROLLER_TAG)
@CrossOrigin(origins = {"http://localhost:3000"})
public class TopRecordsController {

    @Autowired
    private TopRecordsServiceImpl topRecordsService;

    @PostMapping(value = "/getTopRecords", produces = "application/json")
    public ResponseEntity<TopRecordsResDto> getTopRecords(@RequestBody TopRecordsReqDto reqDto) {
        TopRecordsResDto resDto = topRecordsService.createResult(reqDto);
        return ResponseEntity.status(HttpStatus.OK).body(resDto);
    }
}