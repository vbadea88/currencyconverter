/**
 * Philips Electronics N.V. 2020. All rights are reserved. Reproduction or transmission in whole or in part,
 * in any form or by any means, electronic, mechanical or otherwise, is prohibited without the prior written
 * consent of the copyright owner.
 **/

package com.example.demo.mapper;

import com.example.demo.dto.ExchangeEntityReqDto;
import com.example.demo.dto.ExchangeEntityResDto;
import com.example.demo.entities.ExchangeEntity;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * This class is used to handle requests for the ExchangeMapper entity.
 */
@Component
public class ExchangeMapper {

    @Autowired
    private ModelMapper modelMapper;

    private TypeMap<ExchangeEntityReqDto, ExchangeEntity> entityDtoExchangeEntityTypeMap;
    private TypeMap<ExchangeEntity, ExchangeEntityResDto> exchangeEntityResDtoTypeMap;

    public ExchangeEntity convertExcEntDtoToEntity(ExchangeEntityReqDto dto){
        if(entityDtoExchangeEntityTypeMap == null){
            entityDtoExchangeEntityTypeMap = modelMapper.createTypeMap(ExchangeEntityReqDto.class, ExchangeEntity.class);
        }

        return entityDtoExchangeEntityTypeMap.map(dto);
    }

    public ExchangeEntityResDto convertExcEntEntityToRes(ExchangeEntity entity, float amountResult){
        if(exchangeEntityResDtoTypeMap == null){
            exchangeEntityResDtoTypeMap = modelMapper.createTypeMap(ExchangeEntity.class, ExchangeEntityResDto.class);
        }

        ExchangeEntityResDto resDto = exchangeEntityResDtoTypeMap.map(entity);
        resDto.setAmountResult(Float.toString(amountResult));

        return resDto;
    }
}