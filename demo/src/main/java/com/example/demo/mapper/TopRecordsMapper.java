/**
 * Philips Electronics N.V. 2020. All rights are reserved. Reproduction or transmission in whole or in part,
 * in any form or by any means, electronic, mechanical or otherwise, is prohibited without the prior written
 * consent of the copyright owner.
 **/

package com.example.demo.mapper;

import com.example.demo.dto.TopRecordsReqDto;
import com.example.demo.dto.TopRecordsResDto;
import com.example.demo.entities.TopRecordsEntity;
import com.example.demo.utils.DateCurrencyObject;
import com.example.demo.utils.SortEnum;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeMap;
import org.modelmapper.convention.MatchingStrategies;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * This class is used to handle requests for the TopRecordsMapper entity.
 */
@Component
public class TopRecordsMapper {
    @Autowired
    private ModelMapper modelMapper;

    private TypeMap<TopRecordsReqDto, TopRecordsEntity> topRecordsReqDtoToEntityMap;
    private TypeMap<TopRecordsEntity, TopRecordsResDto> topRecordsEntityToResMap;

    public TopRecordsEntity convertTopRecReqToEntity(TopRecordsReqDto reqDto){
        if(topRecordsReqDtoToEntityMap == null){
            modelMapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);
            topRecordsReqDtoToEntityMap = modelMapper.createTypeMap(TopRecordsReqDto.class, TopRecordsEntity.class);
        }

        TopRecordsEntity res = topRecordsReqDtoToEntityMap.map(reqDto);
        res.setOrder(SortEnum.getValueFromString(reqDto.getOrder()).getStringValue());

        return res;
    }

    public TopRecordsResDto convertTopRecEntToRes(TopRecordsEntity ent, List<DateCurrencyObject> dateCurrencyObjects){
        if(topRecordsEntityToResMap == null){
            topRecordsEntityToResMap = modelMapper.createTypeMap(TopRecordsEntity.class, TopRecordsResDto.class);
        }
        TopRecordsResDto resDto = topRecordsEntityToResMap.map(ent);

        resDto.setDateCurrencyObjects(dateCurrencyObjects);
        return resDto;
    }
}