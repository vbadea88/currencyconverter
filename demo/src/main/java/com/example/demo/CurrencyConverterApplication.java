package com.example.demo;

import com.example.demo.utils.ExchangeClass;
import com.example.demo.utils.TrustAllX509TrustManager;
import org.modelmapper.ModelMapper;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import javax.net.ssl.*;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;

@SpringBootApplication
public class CurrencyConverterApplication {

	public static void main(String[] args) throws NoSuchAlgorithmException, KeyManagementException {
		SpringApplication.run(CurrencyConverterApplication.class, args);

		SSLContext sc = SSLContext.getInstance("TLS");
		sc.init(null, new TrustManager[] { new TrustAllX509TrustManager() }, new java.security.SecureRandom());
		HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
		HttpsURLConnection.setDefaultHostnameVerifier( new HostnameVerifier(){
			public boolean verify(String string, SSLSession ssls) {
				return true;
			}
		});
	}

	@Bean
	public ModelMapper createModelMapperBean(){
		return new ModelMapper();
	}

	@Bean
	public ExchangeClass createExchangeClass() {return new ExchangeClass();}
}
