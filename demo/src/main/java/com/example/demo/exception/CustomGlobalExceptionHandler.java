/**
 * Philips Electronics N.V. 2020. All rights are reserved. Reproduction or transmission in whole or in part,
 * in any form or by any means, electronic, mechanical or otherwise, is prohibited without the prior written
 * consent of the copyright owner.
 **/

package com.example.demo.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;

/**
 * This class is used to handle requests for the CustomGlobalExceptionHandler entity.
 */
@ControllerAdvice
public class CustomGlobalExceptionHandler {
    @ExceptionHandler({IOException.class})
    public ResponseEntity<String> handleIOExceptionException(IOException e) {
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getMessage());
    }

    @ExceptionHandler({ParserConfigurationException.class})
    public ResponseEntity<String> handleXmlParseException(ParserConfigurationException e) {
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getMessage());
    }

    @ExceptionHandler({SAXException.class})
    public ResponseEntity<String> handleCustomSaxParseException(SAXException e) {
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getMessage());
    }

    @ExceptionHandler(CurrencyNotSupported.class)
    public ResponseEntity<String> handleCurrencyNotSupported(CurrencyNotSupported e){
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
    }

    @ExceptionHandler(NotSupportedDateException.class)
    public ResponseEntity<String> handleNotSupportedDateException(NotSupportedDateException e){
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
    }

    @ExceptionHandler(SortingOrderTypeException.class)
    public ResponseEntity<String>handleSortingOrderTypeException(SortingOrderTypeException e){
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
    }
}