/**
 * Philips Electronics N.V. 2020. All rights are reserved. Reproduction or transmission in whole or in part,
 * in any form or by any means, electronic, mechanical or otherwise, is prohibited without the prior written
 * consent of the copyright owner.
 **/

package com.example.demo.entities;

/**
 * This class is used to handle requests for the TopRecordsEntity entity.
 */

public class TopRecordsEntity {
    String order;
    String currency;
    Integer nrOfRecords;

    public String getOrder() {
        return order;
    }

    public void setOrder(String order) {
        this.order = order;
    }

    public Integer getNrOfRecords() {
        return nrOfRecords;
    }

    public void setNrOfRecords(Integer nrOfRecords) {
        this.nrOfRecords = nrOfRecords;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }
}