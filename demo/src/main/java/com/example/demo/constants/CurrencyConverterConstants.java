/**
 * Philips Electronics N.V. 2020. All rights are reserved. Reproduction or transmission in whole or in part,
 * in any form or by any means, electronic, mechanical or otherwise, is prohibited without the prior written
 * consent of the copyright owner.
 **/

package com.example.demo.constants;

/**
 * This class is used to handle requests for the CurrencyConverterConstants entity.
 */

public abstract class CurrencyConverterConstants {
    public static final String CURRENCY_NOT_SUPPORTED_EXCEPTION = "The [%s] currency is not supported by BNR!";

    private CurrencyConverterConstants() {
    }
}