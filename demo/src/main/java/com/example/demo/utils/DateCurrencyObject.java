/**
 * Philips Electronics N.V. 2020. All rights are reserved. Reproduction or transmission in whole or in part,
 * in any form or by any means, electronic, mechanical or otherwise, is prohibited without the prior written
 * consent of the copyright owner.
 **/

package com.example.demo.utils;

import java.util.Date;

/**
 * This class is used to handle requests for the OrderObject entity.
 */

public class DateCurrencyObject {
    Date date;
    Float currencyValue;

    public DateCurrencyObject(Date date, Float currencyValue) {
        this.date = date;
        this.currencyValue = currencyValue;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Float getCurrency() {
        return currencyValue;
    }

    public void setCurrency(Float currency) {
        this.currencyValue = currency;
    }
}