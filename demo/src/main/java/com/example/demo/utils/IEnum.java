package com.example.demo.utils;

import com.example.demo.exception.SortingOrderTypeException;

public interface IEnum {

    static <T extends Enum<T> & IEnum> T getEnumFromString(Class<T> c, String value) {
        T[] values = c.getEnumConstants();
        for (T v : values) {
            if (v.getStringValue().equalsIgnoreCase(value)) {
                return v;
            }
        }

        throw new SortingOrderTypeException(String.format("[%s] enum does not have value [%s], please use topMin or topMax.", c.getSimpleName(), value));
    }

    String getStringValue();
}
