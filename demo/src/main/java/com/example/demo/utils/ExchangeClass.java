/**
 * Philips Electronics N.V. 2020. All rights are reserved. Reproduction or transmission in whole or in part,
 * in any form or by any means, electronic, mechanical or otherwise, is prohibited without the prior written
 * consent of the copyright owner.
 **/

package com.example.demo.utils;

import org.apache.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * This class is used to handle requests for the ExchangeClass entity.
 */

public class ExchangeClass {
    private Map<Date, Map<String, Float>> dateRateCurrencyMap = new HashMap<>();

    private static final Logger logger = Logger.getLogger(ExchangeClass.class);

    public void createExchangeMap(String url) throws ParserConfigurationException, SAXException, ParseException, IOException {
        dateRateCurrencyMap.clear();
        HttpURLConnection con = createHttpConnection(url);
        parseXML(con);
    }

    public HttpURLConnection createHttpConnection(String urlString) throws IOException {
        URL url = new URL(urlString);
        HttpURLConnection con = (HttpURLConnection) url.openConnection();
        con.setRequestMethod("GET");
        return con;
    }

    public void parseXML(HttpURLConnection con) throws ParserConfigurationException, IOException, SAXException, ParseException {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        //Document document = builder.parse(new File("D:\\Tutoriale\\CurrencyConverterBitbucket\\currencyconverter\\demo\\src\\main\\java\\com\\example\\demo\\utils\\rates.xml"));
        Document document = builder.parse(con.getInputStream());
        document.getDocumentElement().normalize();

        Element root = document.getDocumentElement();
        NodeList dataSetChilds = root.getChildNodes();
        Node bodyNode = getTargetNode(dataSetChilds, "Body");


        if (bodyNode != null) {
            Node cubeNode = null;
            for(int node = 0 ; node < bodyNode.getChildNodes().getLength(); node++)
            {
                cubeNode = bodyNode.getChildNodes().item(node);
                if(cubeNode.getNodeName().equals("Cube"))
                {
                    HashMap<String, Float> rateMap = new HashMap<>();
                    parseCubeNodes(cubeNode, rateMap);
                }
            }
        }
    }

    private void parseCubeNodes(Node cubeNode, Map<String, Float> rateMap) throws ParseException {
        String currentDate;
        NodeList childCubeNodes;
        currentDate = cubeNode.getAttributes().item(0).getNodeValue();

        childCubeNodes = cubeNode.getChildNodes();
        for (int rateNodeContor = 0; rateNodeContor < childCubeNodes.getLength(); rateNodeContor++) {
            Node rateNode = childCubeNodes.item(rateNodeContor);
            if (rateNode.getNodeName().equals("Rate") && rateNode.hasAttributes()) {
                rateMap.put(rateNode.getAttributes().item(0).getNodeValue(), Float.valueOf(rateNode.getFirstChild().getNodeValue()));
            }
        }

        updateDateRateCurrencyMap(currentDate, rateMap);
    }

    public void updateDateRateCurrencyMap(String currentDate, Map<String, Float> rateMap) throws ParseException {
        DateFormat formatter;
        formatter = new SimpleDateFormat("yyyy-MM-dd");
        dateRateCurrencyMap.put(formatter.parse(currentDate), rateMap);

        if(logger.isDebugEnabled()){
            logger.debug("Added to hashMap date: " + currentDate);
        }
    }

    public Node getTargetNode(NodeList nodeList, String value) {
        Node resultNode = null;
        for (int nodeListChild = 0; nodeListChild < nodeList.getLength(); nodeListChild++) {
            resultNode = nodeList.item(nodeListChild);
            if (resultNode.getNodeName().equals(value)) {
                return resultNode;
            }
        }
        return resultNode;
    }

    public  Map<Date, Map<String, Float>> getDateRateCurrencyMap() {
        return dateRateCurrencyMap;
    }
}