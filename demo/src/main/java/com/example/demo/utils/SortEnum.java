/**
 * Philips Electronics N.V. 2020. All rights are reserved. Reproduction or transmission in whole or in part,
 * in any form or by any means, electronic, mechanical or otherwise, is prohibited without the prior written
 * consent of the copyright owner.
 **/

package com.example.demo.utils;

/**
 * This class is used to handle requests for the SortEnum entity.
 */

public enum SortEnum implements IEnum {
    MIN("topMin"),
    MAX("topMax");

    String criteria;

    SortEnum(String criteria) {
        this.criteria = criteria;
    }

    @Override
    public String getStringValue() {
        return criteria;
    }

    public static SortEnum getValueFromString(String target){
        return IEnum.getEnumFromString(SortEnum.class, target);
    }
}