/**
 * Philips Electronics N.V. 2020. All rights are reserved. Reproduction or transmission in whole or in part,
 * in any form or by any means, electronic, mechanical or otherwise, is prohibited without the prior written
 * consent of the copyright owner.
 **/

package com.example.demo.service.impl;

import com.example.demo.dto.TopRecordsReqDto;
import com.example.demo.dto.TopRecordsResDto;
import com.example.demo.entities.TopRecordsEntity;
import com.example.demo.mapper.TopRecordsMapper;
import com.example.demo.service.TopRecordsService;
import com.example.demo.utils.DateCurrencyObject;
import com.example.demo.utils.ExchangeClass;
import com.example.demo.utils.SortEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

/**
 * This class is used to handle requests for the TopRecordsServiceImpl entity.
 */
@Service
public class TopRecordsServiceImpl implements TopRecordsService {
    @Autowired
    private ExchangeClass exchangeClass;

    @Autowired
    private TopRecordsMapper topRecordsMapper;

    public TopRecordsServiceImpl(ExchangeClass exchangeClass, TopRecordsMapper topRecordsMapper) {
        this.exchangeClass = exchangeClass;
        this.topRecordsMapper = topRecordsMapper;
    }

    @Override
    public TopRecordsResDto createResult(TopRecordsReqDto recordsReqDto) {
        TopRecordsEntity topRecordsEntity = topRecordsMapper.convertTopRecReqToEntity(recordsReqDto);
        TreeMap<Float, Date> orderFloatDateTreeMap = createSortTreeMap(exchangeClass.getDateRateCurrencyMap(), topRecordsEntity.getCurrency());

        List<DateCurrencyObject> dateCurrencyObjects = new ArrayList<>();
        int nrOfrec = 0;
        if (topRecordsEntity.getOrder().equals(SortEnum.MIN.getStringValue())) {
            for (Map.Entry entry : orderFloatDateTreeMap.entrySet()) {
                if (nrOfrec >= topRecordsEntity.getNrOfRecords())
                    break;
                updateDateCurrencyObjectList(dateCurrencyObjects, entry);
                nrOfrec++;
            }
        }

        if (topRecordsEntity.getOrder().equals(SortEnum.MAX.getStringValue())) {
            for (Map.Entry entry : orderFloatDateTreeMap.descendingMap().entrySet()) {
                if (nrOfrec >= topRecordsEntity.getNrOfRecords())
                    break;
                updateDateCurrencyObjectList(dateCurrencyObjects, entry);
                nrOfrec++;
            }
        }

        return topRecordsMapper.convertTopRecEntToRes(topRecordsEntity, dateCurrencyObjects);
    }

    private void updateDateCurrencyObjectList(List<DateCurrencyObject> dateCurrencyObjects, Map.Entry entry) {
        Date date = (Date) entry.getValue();
        Float value = (Float) entry.getKey();
        dateCurrencyObjects.add(new DateCurrencyObject(date, value));
    }

    private TreeMap<Float, Date> createSortTreeMap(Map<Date, Map<String, Float>> exchangeDateRateMap, String currency) {
        TreeMap<Float, Date> valueDateTreeMap = new TreeMap<>();

        Iterator dateRateIterator = exchangeDateRateMap.entrySet().iterator();
        while (dateRateIterator.hasNext()) {
            createOrderMap(dateRateIterator, valueDateTreeMap, currency);
        }

        return valueDateTreeMap;
    }

    private void createOrderMap(Iterator dateRateIterator, Map<Float, Date> valueDateTreeMap, String currency) {
        HashMap<String, Float> rateMap;
        Map.Entry currentElement = (Map.Entry) dateRateIterator.next();
        Date currentDate = (Date) currentElement.getKey();
        rateMap = (HashMap<String, Float>) currentElement.getValue();
        Float currentRate = rateMap.get(currency);

        valueDateTreeMap.put(currentRate, currentDate);
    }

}