package com.example.demo.service;

import com.example.demo.dto.TopRecordsReqDto;
import com.example.demo.dto.TopRecordsResDto;

public interface TopRecordsService {
    public TopRecordsResDto createResult(TopRecordsReqDto recordsReqDto);
}
