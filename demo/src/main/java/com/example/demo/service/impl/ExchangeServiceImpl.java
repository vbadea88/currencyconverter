/**
 * Philips Electronics N.V. 2020. All rights are reserved. Reproduction or transmission in whole or in part,
 * in any form or by any means, electronic, mechanical or otherwise, is prohibited without the prior written
 * consent of the copyright owner.
 **/

package com.example.demo.service.impl;

import com.example.demo.constants.CurrencyConverterConstants;
import com.example.demo.dto.ExchangeEntityReqDto;
import com.example.demo.dto.ExchangeEntityResDto;
import com.example.demo.entities.ExchangeEntity;
import com.example.demo.entities.UrlStringEntity;
import com.example.demo.exception.CurrencyNotSupported;
import com.example.demo.exception.NotSupportedDateException;
import com.example.demo.mapper.ExchangeMapper;
import com.example.demo.service.ExchangeService;
import com.example.demo.utils.ExchangeClass;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.text.ParseException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * This class is used to handle requests for the ExchangeServiceImpl entity.
 */

@Service
public class ExchangeServiceImpl implements ExchangeService {

    @Autowired
    private ExchangeMapper exchangeMapper;

    @Autowired
    private ExchangeClass exchangeClass;

    public ExchangeServiceImpl(ExchangeMapper exchangeMapper, ExchangeClass exchangeClass) {
        this.exchangeMapper = exchangeMapper;
        this.exchangeClass = exchangeClass;
    }

    @Override
    public ExchangeEntityResDto  exchangeResult(ExchangeEntityReqDto reqDto) {
        ExchangeEntity exchangeEntity = exchangeMapper.convertExcEntDtoToEntity(reqDto);

        Map<Date, Map<String, Float>> dateRateMap = exchangeClass.getDateRateCurrencyMap();
        Map<String, Float> rateMap = dateRateMap.get(exchangeEntity.getCurrentDate());

        if (rateMap == null) {
            throw new NotSupportedDateException(String.format("The requested date [%s] is not in the current rateMap hashmap!", exchangeEntity.getCurrentDate()));
        }

        Float conversionResult = getConversionResult(exchangeEntity, rateMap);

        return exchangeMapper.convertExcEntEntityToRes(exchangeEntity, conversionResult);
    }

    private Float getConversionResult(ExchangeEntity exchangeEntity, Map<String, Float> exchangeMap) {

        validateExchangeEntityCurrency(exchangeEntity, exchangeMap);

        Float conversionResult = 0.0F;
        Float comissionFactor = (1 - exchangeEntity.getComission() / 100);

        if (exchangeEntity.getFromCurrency().equals(exchangeEntity.getToCurrency())) {
            conversionResult = exchangeEntity.getAmount();
        } else if (exchangeEntity.getFromCurrency().equals("RON"))
            conversionResult = (exchangeEntity.getAmount()) / exchangeMap.get(exchangeEntity.getToCurrency());
        else if (exchangeEntity.getToCurrency().equals("RON"))
            conversionResult = (exchangeEntity.getAmount() * exchangeMap.get(exchangeEntity.getFromCurrency()));
        else {
            Float amountToRon = (exchangeEntity.getAmount() * exchangeMap.get(exchangeEntity.getFromCurrency()));
            conversionResult = amountToRon / exchangeMap.get(exchangeEntity.getToCurrency());
        }

        return conversionResult * comissionFactor;
    }

    private void validateExchangeEntityCurrency(ExchangeEntity exchangeEntity, Map<String, Float> exchangeMap) {
        if(exchangeEntity.getFromCurrency().equals("RON") || exchangeEntity.getToCurrency().equals("RON"))
            return;

        if (!exchangeMap.containsKey(exchangeEntity.getFromCurrency())) {
            throw new CurrencyNotSupported(String.format(CurrencyConverterConstants.CURRENCY_NOT_SUPPORTED_EXCEPTION,
                    exchangeEntity.getFromCurrency()));
        } else if (!exchangeMap.containsKey(exchangeEntity.getToCurrency())) {
            throw new CurrencyNotSupported(String.format(CurrencyConverterConstants.CURRENCY_NOT_SUPPORTED_EXCEPTION,
                    exchangeEntity.getToCurrency()));
        }
    }

    @Override
    public void createDateRateMap(UrlStringEntity url) throws ParserConfigurationException, SAXException, ParseException, IOException {
        exchangeClass.createExchangeMap(url.getUrl());
    }
}
