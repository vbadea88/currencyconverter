package com.example.demo.service;

import com.example.demo.dto.ExchangeEntityReqDto;
import com.example.demo.dto.ExchangeEntityResDto;
import com.example.demo.entities.UrlStringEntity;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.text.ParseException;

public interface ExchangeService {
    public ExchangeEntityResDto exchangeResult(ExchangeEntityReqDto reqDto) throws ParserConfigurationException, SAXException, IOException, ParseException;
    public void createDateRateMap(UrlStringEntity url) throws ParserConfigurationException, SAXException, ParseException, IOException;
}
