/**
 * Philips Electronics N.V. 2020. All rights are reserved. Reproduction or transmission in whole or in part,
 * in any form or by any means, electronic, mechanical or otherwise, is prohibited without the prior written
 * consent of the copyright owner.
 **/

package com.example.demo.dto;

import lombok.Data;
import lombok.Value;

/**
 * This class is used to handle requests for the ExchangeEntityResDto entity.
 */

@Data
public class ExchangeEntityResDto {
    private String currentDate;
    private String fromCurrency;
    private String amount;
    private String toCurrency;
    private String amountResult;
    private String comission;
}