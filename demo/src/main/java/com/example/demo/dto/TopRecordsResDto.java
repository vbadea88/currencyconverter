/**
 * Philips Electronics N.V. 2020. All rights are reserved. Reproduction or transmission in whole or in part,
 * in any form or by any means, electronic, mechanical or otherwise, is prohibited without the prior written
 * consent of the copyright owner.
 **/

package com.example.demo.dto;

import com.example.demo.utils.DateCurrencyObject;
import lombok.Builder;
import lombok.Data;
import lombok.Value;

import java.util.List;

/**
 * This class is used to handle requests for the TopRecordsResDto entity.
 */

@Data
public class TopRecordsResDto {
    private String order;
    private String currency;
    private List<DateCurrencyObject> dateCurrencyObjects;
}